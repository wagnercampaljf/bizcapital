<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

$this->title = 'Avaliação BizCapital';
?>
<h2><b>Recomendar pratos em ...</b></h2>

<?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-11"><?= $form->field($model, 'nome')->label(false) ?></div><div class="col-1"><?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?></div>
    </div>

    <?php if(isset($erro)){?>
        <div class="container">
            <div class="row">
                <h3><b text-color=red>Erro: <?= $erro?></b></h3>
            </div>
        </div>
    <?php } ?>

    <?php if(isset($cidade)){?>
        <div class="container">
            <div class="row">
                <h3><b>Cidade: <?= $cidade?></b></h3><br><h3><b>Temperatura: <?=$temperatura ?> C</b></h3>
            </div>
            <div class="row">
                <?php
                    foreach($pratos as $k => $dicas){
                        echo '<div class="col"><div class="row"><div class="col"><b>'.$k.'</b></div></div>';

                        foreach ($dicas as $i => $dica) {
                            echo '<div class="row"><div class="col">'.$dica.'</div></div>';
                        }
                        //echo "<pre>"; print_r($prato); echo "</pre>";
                        echo "</div>";
                    }
                ?>
            </div>
        </div>
    <?php } ?>

<?php ActiveForm::end(); ?>

<div class="container" style="margin-top:50px">
    <div class="row justify-content-center">
        <div class="col-2"><b>Últimas Buscas</b></div>
    </div>                
    <div class="row">
        <div class="col">
            <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => null,//$searchModel,
                    //'filter'=>null,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'label'     => 'Busca',
                            'attribute' => 'descricao',
                            'value'     => 'descricao',
                        ],
                        [
                            'label'     => 'Buscado em',
                            'attribute' => 'data_hora_busca',
                            'value'     => 'data_hora_busca',
                        ],
                        'principais_pratos'
                    ],
                ]); ?>
        </div>
    </div>
</div>

