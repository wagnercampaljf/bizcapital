<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\CidadeForm;
use app\models\Log;
use app\models\LogSearch;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //$logs = Log::find()->orderBy(["data_hora_busca" => SORT_DESC])->limit(20)->all();
        
        $searchModel = new LogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new CidadeForm();
        if($model->load(Yii::$app->request->post())){
            $url        = str_replace(" ", "%20", "http://localhost/bizcapital_webservice/web/?r=site/obter-pratos-por-cidade&cidade=".$model->nome);
            $ch         = curl_init( $url );

            //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            $info   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $return["body"]   = json_decode($result, JSON_UNESCAPED_UNICODE);
            $return["httpCode"] = $info;
            $return["urlTeste"] = $url;
            curl_close($ch);

            if(array_key_exists("erro", $return["body"])){
                return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'model' => $model, "erro" => $return["body"]["erro"]]);
            }

            if($return["httpCode"] = 200){

                $log                    = new Log;
                $log->descricao         = $return["body"]["cidade"];
                $log->data_hora_busca   = date("Y-m-d H:i:s");

                $pratos = "";
                foreach($return["body"]["pratos"] as $k => $pratos_array){
                    foreach($pratos_array as $i => $prato){
                        
                        if($i > 2){
                            break;
                        }
    
                        $pratos .= $prato.",";
                    }

                    break;
                }

                $pratos                 = rtrim($pratos,",");
                $log->principais_pratos = $pratos;
                $log->save();

                return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'model' => $model, 'cidade' => $return["body"]["cidade"], 'temperatura' => $return["body"]["temperatura"], 'pratos' => $return["body"]["pratos"]]);
            }
            //echo "<br><br><br><br><br><pre>"; print_r($return); echo "</pre>";

        }

        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'model' => $model]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionObterTemperatura(){
        echo 111;
    }
}
