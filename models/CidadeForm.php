<?php

namespace app\models;

use Yii;
use yii\base\Model;

class CidadeForm extends Model
{
    public $nome;

    public function rules()
    {
        return [
            [['nome'], 'required'],
        ];
    }
}